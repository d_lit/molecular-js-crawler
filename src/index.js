import app from './app';
import test from './test';

try {
  app.start().then(() => test(app));
} catch (error) {
  console.error(error);
}

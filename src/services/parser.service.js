import parse from '../modules/parser';

export default {
  name: 'parser',
  actions: {
    parse({ params: { url, role, season }}) {
      return parse(url, role, season);
    }
  }
};

import reduce from 'lodash/reduce';

const parseDOM = (document, selector, format) => {
  switch (typeof selector) {
    case 'string': {
      switch (format) {
        case 'html': {
          return $(document).find(selector).html();
        }
        case 'text': {
          return $(document).find(selector).text();
        }
        default:
        case 'raw':
        case 'undefined': {
          return $(document).find(selector);
        }
      }
    }
    case 'function': {
      return selector(document);
    }
    case 'object': {
      const obj = selector;
      return reduce(obj, (result, selector, key) => {
        result[key] = parseDOM(
          document,
          selector,
          format,
        );
        return result;
      }, {});
    }
    default: {
      break;
    }
  }
};

export default parseHTML;

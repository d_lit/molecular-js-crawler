import { Chromeless } from 'chromeless';

export default async (url, wait = 'body') => {
  try {
    const browser = new Chromeless();

    const html = await browser
      .goto(url)
      .wait(wait)
      .html();

    await browser.end();

    return html;
  } catch (error) {
    throw (error);
  }
};

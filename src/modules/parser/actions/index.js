import parseHTML from './parseHTML';
import parseEntity from './parseEntity';
import parseCollection from './parseCollection';

export default {
  parseHTML,
  parseEntity,
  parseCollection,
};

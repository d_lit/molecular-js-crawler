export default (rows, groupRowClassName) => {
  let lastGroupRowKey = '';

  return rows.reduce((memo, row) => {
    if (row.className.includes(groupRowClassName)) {
      lastGroupRowKey = $(row).text().trim();
      return {
        ...memo,
        [lastGroupRowKey]: [],
      };
    } else {
      return {
        ...memo,
        [lastGroupRowKey]: [
          ...memo[lastGroupRowKey],
          row.outerHTML,
        ],
      };
    }
  }, {});
};

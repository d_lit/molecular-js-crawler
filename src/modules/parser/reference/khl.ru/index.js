import fs from 'fs';
import get from 'lodash/get';
import reduce from 'lodash/reduce';

import hostActions from './actions';
import hostSelectors from './selectors';

export default async ({
  url,
  role,
  // season,
  actions: {
    openUrl,
    parseHTML,
    parseCollection,
  },
  // util,
}) => {
  /* HTML */
  const html = await openUrl(url);

  /* Selectors */
  const statsRowsSelector = get(
    hostSelectors, ['pages', 'playerStats', 'statsTableRows'],
  );

  const groupRowClassName = get(
    hostSelectors, ['pages', 'playerStats', 'groupRowClassName'],
  );

  const playerRoleStatsModel = get(
    hostSelectors, ['roles', role, 'statsTableCells'],
  );

  /* Actions */
  const statsTableRows = parseHTML(html, statsRowsSelector);

  const groupedStatsRows = hostActions.groupRows(
    Array.from(statsTableRows), groupRowClassName,
  );

  // TODO: Добавить парсер parseGroupedCollection
  const json = reduce(groupedStatsRows, (result, row, key) => ({
    ...result, [key]: parseCollection(row, playerRoleStatsModel, 'text'),
  }), {});

  // TODO: Добавить фильтр для текущего сезона
  console.log('json');
  console.dir(json);
};

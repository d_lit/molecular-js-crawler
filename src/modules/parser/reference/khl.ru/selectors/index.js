import pages from './pages';
import roles from './roles';

export default {
  pages,
  roles,
};

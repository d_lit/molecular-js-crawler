const selectors = {
  statsTableRows: 'table#pl_Stats tbody tr',
  groupRowClassName: 'group',
};

export default {
  ...selectors,
};

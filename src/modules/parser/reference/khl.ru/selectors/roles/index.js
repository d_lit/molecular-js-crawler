import fieldplayer from './fieldplayer';
import goalkeeper from './goalkeeper';

export default {
  fieldplayer,
  goalkeeper,
};

const statsTableCells = {
  'Команда': 'td:nth-child(1)',
  '№': 'td:nth-child(2)',
  'И': 'td:nth-child(3)',
  // ...
};

export default {
  statsTableCells,
};

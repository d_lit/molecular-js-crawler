import khl from './khl.ru';

import extractRootDomain from '../util/extractRootDomain';

const parsers = {
  'khl.ru': khl,
};

export default (url) => {
  const hostname = extractRootDomain(url);
  const parser = parsers[hostname];
  return parser;
};

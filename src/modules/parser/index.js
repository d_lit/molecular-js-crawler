import hosts from './hosts';
import openUrl from './browser';
import actions from './actions';
// import util from './util;
// import selectors from './selectors;

export default (url, role, season) => {
  const parser = hosts(url);

  return parser({
    url,
    role,
    season,
    actions: {
      openUrl, ...actions
    },
    // util,
    // selectors,
  });
};

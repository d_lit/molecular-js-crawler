/* respects => https://gist.github.com/RavenHursT/fe8a95a59109096ac1f8 */
export default url =>
  url
    .match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i)[1]
    .split('.')
    .slice(-2)
    .join('.');

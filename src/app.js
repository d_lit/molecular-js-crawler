import { ServiceBroker } from 'moleculer';

import services from './services';

const broker = new ServiceBroker({
  logger: console,
});

const initService = service => broker.createService(service);

export const initServices = (services) => services.forEach(
  service => initService(service),
);

export const init = (services = []) => {
  initServices(services);
  return broker;
};

export default init(services);
